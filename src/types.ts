import { Updater, Writable } from 'svelte/store';

export interface Ayah {
	id: number;
	chapter_id: number;
	juz_number: number;
	text_uthmani: string;
	v1_page: number;
	verse_number: number;
}

export interface Juz {
	id: number;
	juz_number: number;
	first_verse_id: number;
	last_verse_id: number;
	verses_count: number;
	verse_mapping: Record< string, `${ number }-${ number }` >;
}

export interface Surah {
	bismillah_pre: boolean;
	id: number;
	name_arabic: string;
	pages: number[];
	revelation_place: string;
	revelation_text?: string;
	verses_count: number;
}

export interface Pagination {
	current_page: number;
	next_page: number | null;
	per_page: number;
	total_pages: number;
	total_records: number;
}

export interface VerseByPage {
	pagination: Pagination;
	verses: Ayah[];
}

export interface ChaptersEndpoint {
	chapters: Surah[];
}

export interface JuzEndpoint {
	juzs: Juz[];
}

export interface JuzItem {
	number: number;
	page_number: number;
	surahs: Surah[];
}

/* --- */

export type RevelationTypes = Record< string, string >;

export interface Page {
	id: number;
	ayahs: number[];
	surahs: number[];
	surah_of_page: number;
}

export interface Data {
	PAGE_NUMBERS: number[];
	REVELATION_TYPES: RevelationTypes;
	TOTAL_PAGES: number;
	ayahs: Ayah[];
	surahs: Surah[];
	juzs: Juz[];
	juz_list: JuzItem[];
	pages: Page[];
	page_ayahs?: Ayah[];
	page_surahs?: Surah[];
	surah_of_page?: number;
}

export interface BookmarkItem {
	is_last_open?: boolean;
	link: string;
	page_number: number;
	text_info: string;
	text_page_number: string;
	text_surah: string;
}

/* --- stores --- */
export interface WithInitial< T > extends Omit< Writable< T >, 'update' > {
	get( this: void ): T;
	reset( this: WithInitial< T > ): void;
	update( this: WithInitial< T >, updater: Updater< T > | Object ): void;
}

export interface Bookmarks {
	last_open: number;
	pages: number[];
}

export interface BookmarkStore extends WithInitial< Bookmarks > {
	add_page( this: WithInitial< Bookmarks >, page_number: number ): void;
	remove_page( this: WithInitial< Bookmarks >, page_number: number ): void;
}
