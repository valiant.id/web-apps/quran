/**
 * Svelte store backed by window.localStorage
 * Persists store's data locally
 *
 * Credit: https://gist.github.com/joshnuss/aa3539daf7ca412202b4c10d543bc077
 */

import { withInitial } from './with-initial';

/**
 * Wrap a regular writable store
 *
 * @template T
 * @param {string} key     Local storage key.
 * @param {T}      initial Initial data.
 * @return {import("../types").WithInitial<T>} Store with local storage.
 */
export function withLocalStorage( key, initial ) {
	// Create an underlying store.
	const store = withInitial( initial );
	const { set } = store;

	// Get the last value from localStorage.
	const json = window.localStorage.getItem( key );

	// Use the value from localStorage if it exists.
	if ( json ) {
		set( JSON.parse( json ) );
	}

	// Return an object with the same interface as svelte's writable()
	return {
		...store,

		reset() {
			this.set( initial );
		},

		// Capture set and write to localStorage.
		set( value ) {
			const finalValue = {
				...initial,
				...value,
			};

			window.localStorage.setItem( key, JSON.stringify( finalValue ) );
			set( finalValue );
		},
	};
}
