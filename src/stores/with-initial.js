import { get, writable } from 'svelte/store';

/**
 * Create store with initial data
 *
 * @template T
 * @param {T}      initial Initial data.
 * @param {...any} rest    Rest arguments.
 * @return {import("../types").WithInitial<T>} Writable store with initial data.
 */
export function withInitial( initial, ...rest ) {
	const store = writable( initial, ...rest );
	const { set, subscribe } = store;

	return {
		subscribe,

		get() {
			return get( store );
		},

		reset() {
			set( initial );
		},

		set( value ) {
			set( {
				...initial,
				...value,
			} );
		},

		update( arg ) {
			const current = this.get();
			const value = typeof arg === 'function' ? arg( current ) : { ...current, ...arg };

			this.set( value );
		},
	};
}
